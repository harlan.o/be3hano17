CREATE DATABASE db_apigo;
CREATE USER `user1`@`localhost` IDENTIFIED BY '12345abcABC^&*';
USE db_apigo;
GRANT ALL PRIVILEGES ON db_apigo.* TO `user1`@`localhost`;
FLUSH PRIVILEGES;

CREATE TABLE IF NOT EXISTS `dailytask` (
  `id_dt` varchar(50) NOT NULL,
  `title_dt` varchar(255) NOT NULL,
  `description_dt` text NOT NULL,
  `type_dt` varchar(10) NOT NULL,
  `person_dt` varchar(20) DEFAULT 'Person',
  `group_dt` varchar(20) DEFAULT 'Group',
  `status_dt` varchar(10) NOT NULL,
  `start_date_dt` datetime NOT NULL,
  `dateline_dt` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id_dt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `teamwork` (
	`id_tw` VARCHAR(20) NOT NULL,
	`name_tw` VARCHAR(255) NOT NULL,
	`description_tw` TEXT NOT NULL,
	`leader_tw` VARCHAR(20) NOT NULL,
	`status_tw` VARCHAR(10) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	PRIMARY KEY (`id_tw`)
)
COLLATE='utf8mb4_general_ci'
;

CREATE TABLE `member` (
	`id_membr` INT NOT NULL AUTO_INCREMENT,
	`id_tw` VARCHAR(20) NOT NULL,
	`id_user` VARCHAR(20) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	PRIMARY KEY (`id_membr`)
)
COLLATE='utf8mb4_general_ci'
;

-- https://pastebin.com/dNaT8fHG 
