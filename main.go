package main

import (
	"be3hano17/config"
	"be3hano17/dailytask"
	"be3hano17/model"
	"be3hano17/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)
const addr = `:1717`
type Server struct {
	db      *sql.DB
	ViewDir string
} 
// Harlan Noel Oroh
type Message struct {
    Type string
    Status bool
    Message string
}

type handler func(w http.ResponseWriter, r *http.Request)
func InitServer() *Server {
	db, err := config.Mysql()
	if err != nil {
		log.Fatal(err)
	}
	return &Server{
		db:      db,
		ViewDir: `views/`,
	}
}
func (s *Server) Listen() {
	log.Println(`listen at `+addr)
	http.HandleFunc(`/supervisor/create-dailytask`,s.SpvCreateDailyTask())
	http.HandleFunc(`/staff/update-status-dailytask`,s.StaffUpdateStatusDailyTask())
	err := http.ListenAndServe(addr,nil)
	if err != nil {
		fmt.Println(err)
	}
}


func (s *Server) SpvCreateDailyTask() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == `GET` {
			http.ServeFile(w,r,s.ViewDir+`spv_dailytask_create.html`)
			return
		}
		dt := model.Dailytask{}
		err := json.NewDecoder(r.Body).Decode(&dt)
		if utils.IsError(w,err) {
			return
		}
		err = dailytask.Insert(s.db,&dt)
		if utils.IsError(w,err) {
			return
		}
		utils.ResponseJson(w, dt)
	}
}


func (s *Server) StaffUpdateStatusDailyTask() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == `GET` {
			http.ServeFile(w,r,s.ViewDir+`staff_dailytask_status_update.html`)
			return
		}
		m := model.Dailytask{}
		err := json.NewDecoder(r.Body).Decode(&m)
		if utils.IsError(w,err) {
			return
		}
		mes:= Message{}
		mes.Type = "Update"
		var msg string
		msg, err = dailytask.UpdateStatus(s.db,&m)
		if utils.IsError(w,err) {
			mes.Status= false
			return
		}
		mes.Message = msg
		mes.Status= true 
		utils.ResponseJson(w, mes)
	}
}



func main() {
	server := InitServer()
	server.Listen()
}
