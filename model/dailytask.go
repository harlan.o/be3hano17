package model

import "time"

type Dailytask struct {
	
	Id_dt 				string `json:"id_dt"`
	Title_dt			string `json:"title_dt"`
	Description_dt		string `json:"description_dt"`
	Type_dt				string `json:"type_dt"`
	Person_dt			string `json:"person_dt"`
	Group_dt			string `json:"group_dt"`
	Status_dt			string `json:"status_dt"`
	Start_date_dt		time.Time `json:"start_date_dt"`
	Dateline_dt			time.Time `json:"dateline_dt"`
	Created_at 			time.Time `json:"created_at"`
	Updated_at 			time.Time `json:"updated_at"`

}
