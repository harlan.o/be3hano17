package dailytask

import (
	"be3hano17/model"
	"database/sql"
	"fmt"
	"time"
	"log"
)

const table = `dailytask`
const dateformat = `2006-01-02 15:04:05`
const iduserlogin= `1212`

func Insert(db *sql.DB, dt *model.Dailytask) (err error) {
	now := time.Now()
	dateTime :=now.Format("20060102150405")
	Id_dt := dateTime+"U"+iduserlogin
	var id_type_temp string
	var sql string
	if dt.Type_dt == "Group" {
		sql = fmt.Sprintf(`INSERT INTO %v (id_dt, title_dt, description_dt, type_dt, group_dt, start_date_dt, dateline_dt, status_dt, created_at, updated_at) 
		VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, table)
		id_type_temp = dt.Group_dt

	}else if dt.Type_dt == "Person"{
		sql = fmt.Sprintf(`INSERT INTO %v (id_dt, title_dt, description_dt, type_dt, person_dt, start_date_dt, dateline_dt, status_dt, created_at, updated_at) 
		VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, table)
		id_type_temp = dt.Person_dt
	} 
	// fmt.Println(dt.Type_dt)
	res, err := db.Query(sql, Id_dt, dt.Title_dt, dt.Description_dt, dt.Type_dt, id_type_temp, dt.Start_date_dt, dt.Dateline_dt, dt.Status_dt, now, now)
	if err != nil {
		return err
	}
	defer res.Close()
	dt.Id_dt = Id_dt
	dt.Created_at = now
	dt.Updated_at = now
	return
}


func UpdateStatus(db *sql.DB, dt *model.Dailytask) (msg string, err error){
	var dailytask model.Dailytask
	dailytask, err = Select(db,dt)
	sql := fmt.Sprintf(`UPDATE  %v SET status_dt=?, updated_at=?  WHERE id_dt=?`, table)
	now := time.Now()
	
	res, err := db.Query(sql, dt.Status_dt, now, dt.Id_dt)
	if err != nil {
		return "Error", err
	}
	defer res.Close()
	if err != nil {
		return "Error", err
	}
	msg= dailytask.Status_dt + " menjadi " + dt.Status_dt
	return
}

func Select(db *sql.DB, dt *model.Dailytask) ( dailytask model.Dailytask, err error){
	sql := fmt.Sprintf(`SELECT * FROM %s WHERE id_dt=? ORDER BY id_dt DESC`, table)
	row := db.QueryRow(sql, dt.Id_dt)


	start_date, dateline, createdAt, updatedAt := ``, ``,``,``
		err = row.Scan(&dailytask.Id_dt,
			&dailytask.Title_dt,
			&dailytask.Description_dt,
			&dailytask.Type_dt,
			&dailytask.Person_dt,
			&dailytask.Group_dt,
			&dailytask.Status_dt,
			&start_date,
			&dateline,
			&createdAt,
			&updatedAt)
			fmt.Println(&dailytask.Status_dt)
			if err != nil {
				log.Fatal(err)
			}
			dailytask.Start_date_dt, _ = time.Parse(dateformat, start_date)
			dailytask.Dateline_dt, _ = time.Parse(dateformat, dateline)
			dailytask.Created_at, _ = time.Parse(dateformat, createdAt)
			dailytask.Updated_at, _ = time.Parse(dateformat, updatedAt)
			
			
		if err !=nil {
			return dailytask, err
		}		
	return
}